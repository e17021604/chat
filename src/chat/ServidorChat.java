
package chat;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import javax.swing.DefaultListModel;

/**
 * @author Daniel
 *
 */
public class ServidorChat{
    private DefaultListModel charla = new DefaultListModel();

    
    public static void main(String[] args){
        ServidorChat servidorChat = new ServidorChat();
    }

    
    public ServidorChat(){
        try
        {
            ServerSocket socket = new ServerSocket(5557);
            while (true){
                Socket cliente = socket.accept();
                Runnable nuevoCliente = new HiloDeCliente(charla, cliente);
                Thread hilo = new Thread(nuevoCliente);
                hilo.start();
            }
        } catch (IOException e)
        {
        }
    }
}
