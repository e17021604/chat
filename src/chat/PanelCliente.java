package chat;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * @author Daniel
 */
public class PanelCliente
{
    
    private final JScrollPane scroll;

    private final JTextArea textArea;

    private final JTextField textField;

    private final JButton boton;

    
    public PanelCliente(Container contenedor)
    {
        contenedor.setLayout(new BorderLayout());
        textArea = new JTextArea();
        textArea.setEditable(false);
        scroll = new JScrollPane(textArea);

        JPanel panel = new JPanel(new FlowLayout());
        textField = new JTextField(50);
        boton = new JButton("Enviar");
        panel.add(textField);
        panel.add(boton);
        
        

        contenedor.add(scroll, BorderLayout.CENTER);
        contenedor.add(panel, BorderLayout.SOUTH);
    }

    

    
    public void addActionListener(ActionListener accion){
        textField.addActionListener(accion);
        boton.addActionListener(accion);
    }

    
    public void addTexto(String texto)
    {
        textArea.append(texto);
    }

    
    public String getTexto()
    {
        String texto = textField.getText();
        textField.setText("");
        return texto;
    }
}
