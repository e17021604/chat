package chat;

import java.io.IOException;
import java.net.Socket;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

/**
 * 
 * @author Daniel
 *
 */

public class ClienteChat
{
    
    private Socket socket;
    private PanelCliente panel;

    /**
     * @param args
     */
    public static void main(String[] args){
        ClienteChat clienteChat = new ClienteChat();
    }

    
    public ClienteChat(){
        try{
            creaYVisualizaVentana();
            socket = new Socket("192.168.1.65", 5557);
            ControlCliente control = new ControlCliente(socket, panel);
        } catch (IOException e)
        {
            System.out.println("La direccion o el socket fueron introducidos incorrectamente.");
        }
    }

    
    private void creaYVisualizaVentana(){
        JFrame v = new JFrame();
        panel = new PanelCliente(v.getContentPane());
        v.pack();
        v.setVisible(true);
        v.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
}
