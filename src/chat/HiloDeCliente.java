package chat;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import javax.swing.DefaultListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/**
 * @author Daniel
 */
public class HiloDeCliente implements Runnable, ListDataListener{
    private DefaultListModel charla;

   private Socket socket;

    private DataInputStream dataInput;

   private DataOutputStream dataOutput;

    
    public HiloDeCliente(DefaultListModel charla, Socket socket){
        this.charla = charla;
        this.socket = socket;
        try{
            dataInput = new DataInputStream(socket.getInputStream());
            dataOutput = new DataOutputStream(socket.getOutputStream());
            charla.addListDataListener(this);
        } catch (IOException e){
            System.out.println(e);
        }
    }

    
    @Override
    public void run()
    {
        try
        {
            while (true){
                String texto = dataInput.readUTF();
                synchronized (charla)
                {
                    charla.addElement(texto);
                    System.out.println(texto);
                }
            }
        } catch (IOException e)
        {
        }
    }

    
    @Override
    public void intervalAdded(ListDataEvent e){
        String texto = (String) charla.getElementAt(e.getIndex0());
        try
        {
            dataOutput.writeUTF(texto);
        } catch (IOException excepcion)
        {
        }
    }

    @Override
    public void intervalRemoved(ListDataEvent e)
    {
    }

    @Override
    public void contentsChanged(ListDataEvent e)
    {
    }
}
